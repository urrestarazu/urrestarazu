# Iñigo Urrestarazu-Porta

## Ph.D. Candidate

📝 **Personal website:** [urrestarazu.gitlab.io/](https://urrestarazu.gitlab.io/)

I am a Ph.D. candidate on linguistics at the CNRS - IKER UMR 5478, U. of the Basque Country and the U. of Pau and the Country of Adour. For the Ph.D. I work on the phonetics of Basque sibilants from a historical point of view.

My interests:

- Language variation and change

- Phonetic description and documentation

- Historical linguistics (of Basque)

- Dialectology

- Bayesian inference
